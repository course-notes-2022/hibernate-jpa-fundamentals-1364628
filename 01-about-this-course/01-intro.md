# Intro

In this course, we'll learn about **Hibernate** and the **Java Persistence API**
(JPA). We'll start from scratch:

- What is object persistence?
- What is the mismatch between object model and relational model?
- How does object mapping help with this mismatch?

Hibernate can be tricky to get started, but we'll cover all the ground for you
to be a confident Hibernate developer by the end of the course!
