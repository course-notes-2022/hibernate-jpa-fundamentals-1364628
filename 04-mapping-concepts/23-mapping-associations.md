# Mapping Associations

Imagine we have the following entities: a `Guide`, representing a tour guide on
a college campus, and a `Student` taking the tour. One `Guide` can be assigned
to many `Student`s at a time:

![one to many](./screenshots/mapping-associations-one-to-many.png)

## `@ManyToOne`

Let's model our entities in code:

```java
package entity;

import javax.persistence.*;

@Entity
public class Guide (

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Column(name="staff_id", nullable=false)
    private String staffId;

    private String name;
    private Integer salary;

    public Guide(){}

    public Guide(String staffId, String name, Integer salary) {
        this.staffId = staffId;
        this.name = name;
        this.salary = salary;
    }
)
```

```java
package entity;

import javax.persistence.*;

@Entity
public class Student (

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Column(name="enrollment_id", nullable=false)
    private String enrollment;

    private String name;

    private Guide guide;

    public Student(){}

    public Student(String enrollmentId, String name, Guide guide) {
        this.staffId = enrollmentId;
        this.name = name;
        this.guide = guide;
    }
)
```

The `guide` attribute in the `Student` class **is a FOREIGN KEY reference** to a
`Guide` identified by the `id` attribute in the `guide` table:

![one-to-many student guide](./screenshots/one-to-many-student-guide.png)

We can create this relationship using the `@OneToMany` annotation on the
`Student` class:

```java
import javax.persistence.*;

@Entity
public class Student (

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Column(name="enrollment_id", nullable=false)
    private String enrollment;

    private String name;

    @ManyToOne
    @JoinColumn(name="guide_id")
    private Guide guide;

    public Student(){}

    public Student(String enrollmentId, String name, Guide guide) {
        this.staffId = enrollmentId;
        this.name = name;
        this.guide = guide;
    }
);
```

`Student` to `Guide` is a **many-to-one** relationship. The `@JoinColumn`
annotation tells Hibernate that the the `guide_id` column in the `Student` table
should be used as the **foreign key _TO_ the `guide` table**:

![mapping one to many](./screenshots/mapping-one-to-many-2.png)

We need to inform Hibernate about the `Student` and `Guide` entities in our
`hibernate.cfg.xml` file:

![student and guide config](./screenshots/student-and-guide-configt.png)

We can now **persist the student and guide in the database:**

![persist student and guide](./screenshots/persist-student-and-guide.png)

Running this code results in the following records being persisted:

`guide`:

```
| id  | name        | salary | staff_id    |
| --- | ----------- | ------ | ----------- |
| 1   | Mike Lawson | 1000   | 2000MO10789 |
```

`student`

```
| id  | enrollment_id | name       | guide_id |
| --- | ------------- | ---------- | -------- |
| 1   | 2014JT50123   | John Smith | 1        |
```
