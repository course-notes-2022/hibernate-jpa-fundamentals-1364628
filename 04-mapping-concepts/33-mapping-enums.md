# Mapping Enums

Let's look at how to map **Enums**. Imagine the following scenario, where we
have an `Employee` entity with an `employeeStatus` attribute, that could be one
of three values:

- `FULL_TIME`
- `PART_TIME`
- `CONTRACT`

We want to create a table and database record like the following:

![lesson 33 01](./screenshots/lesson-33-01.png)

To do so, we simply add the `@Enumerated(EnumType.STRING)` annotation:

![lesson 33 02](./screenshots/lesson-33-02.png)

Note that we **must** set the EnumType to STRING, otherwise we get the default
behavior, which is to save the **ordinal value** of the Enum in the
`employee_status` column. The ordinal value is the **numerical order of the
string in the enum**, starting from `0, 1, 2...`.

As always, before we can persist data we need to inform Hibernate about our
`Employee` entity in `hibernate.cfg.xml`:

![lesson 33 03](./screenshots/lesson-33-03.png)

## Persisting the Data

We can persist the data as follows:

![lesson 33 04](./screenshots/lesson-33-04.png)
