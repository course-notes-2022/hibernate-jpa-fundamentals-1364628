# Lab Exercise: Cascades

1. Q: What would be the result of executing the following code, assuming the
   state of `student` and `guide` tables is shown in the figure below?

![lab exercise cascades](./screenshots/lab-exercise-cascades.png)

A: Java will throw an exception indicating that our code is violating a
**foreign key constraint**. Hibernate will not allow us to delete a record that
is **still referenced by a foreign key** in another record. **This is a good
thing**, and Hibernate is helping us enforce our database table integrity. In
the above example, Hibernate will not allow us to delete the `Student` with ID
`2`, as doing so would delete the `Guide` that is also associated with **another
`Student`**. To delete student 2, we first \*\*set its `guide` attribute to
`null`:

![set guide to null](./screenshots/set-guide-to-null.png)

**Try to avoid setting `CascadeType.REMOVE` on your `@ManyToOne`
relationships**.
