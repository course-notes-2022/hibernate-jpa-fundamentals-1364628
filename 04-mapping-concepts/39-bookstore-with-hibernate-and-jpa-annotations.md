# Bookstore with Hibernate and JPA Annotations

In this lesson we'll do the ORM for the same bookstore application we worked on
in the first section of the course. In that section, we used JDBC API mapping
then, but this time we'll use **Hibernate**.

Recall that we had 3 **entities**: `Chapter`, `Book`, and `Publisher`, and 3
database tables `chapter`, `book`, and `publisher`:

![lesson 39 01](./screenshots/lesson-39-01.png)

We'll be implementing the following object model, in which the `Book` object
graph contains `Publisher` and `Chapter` objects embedded, and the following
relational model where a `chapter` is related to a `book` by the foreign key of
the `ISBN`, and a `publisher` is related to a `book` by the foreign key of the
`publisher` code:

![lesson 39 02](./screenshots/lesson-39-02.png)
