# Mapping Collections of Value Types

Let's learn how to map a **collection** of value types. In some situations, we
don't really need to associate two entities, but simply create a collection of
basic data types on a **single** entity.

Imagine the following example:

- We have a `Friend` entity with 4 attributes:
  - `id`: Long
  - `name`: String
  - `email`: String
  - `nickNames`: Collection<String>

`String` is a basic Java data type. How do we persist a collection in a
database? We store **each value** in a table, as a **separate record**, and join
on the primary key of the "parent" record:

![lesson 35 01](./screenshots/lesson-35-01.png)

**IMPORTANT**: To avoid duplicate rows in `friend_nickname`, we need a
**composite key** made up of the `friend_id` and the `nickname`.

## Mapping our Data

We can map our data as follows:

![lesson 35 02](./screenshots/lesson-35-02.png)

- `@ElementCollection`: used to indicate we are mapping a collection of simple
  data types

- `@CollectionTable`: indicates that we are storing the collection data **in a
  separate table**:

  - `name="friend_nickname"`: defines the name of the database table
  - `joinColumns=@JoinColumn(name="friend_id")`: defines the name of the
    **foreign key column** in the collection database table

- `@Column`: defines the name of the column holding the data

## Persisting Data

First, let's update our Hibernate config file as usual:

![lesson 35 03](./screenshots/lesson-35-03.png)

We can now persist the data as follows:

![lesson 35 04](./screenshots/lesson-35-04.png)

**Note that we add the composite primary key MANUALLY to the `friend_nickname`
table**.

## Retrieving the Data

Running the following code to **retrieve the data**:

![lesson 35 05](./screenshots/lesson-35-05.png)

yields the following results:

```
Friend [id = 1, name=Mark Anderson, email=markanderson@pluswhere.com, nicknames=[Marco, Markster, Marky]]
```

## Mapping Collections of Embeddable Objects

Note that _we are not limited to mapping basic value types!_ We can map
**embeddable objects** as well.

For example, imagine we have a collection of embeddable `Address` objects that
we want to map. We can achieve this as follows:

![lesson 35 06](./screenshots/lesson-35-06.png)
