# Orphan Removal

In this section, we'll discuss the `orphanRemoval` attribute, and how it can
help avoid the `ConstraintViolationException`.

![lesson 28 01](./screenshots/lesson-28-01.png)

If we attempt to delete the `Student` with id `2`, we expect to delete the
`Guide` with id `2` as well since the `CascadeType.REMOVE` attribute is set on
the `Student` entity. _However_, Hibernate will throw a
`ConstraintViolationException`, since the `Guide` is **still referenced by
`Student` with id `3`**. Otherwise, Student `3` will become an **orphaned
record**:

> An **orphaned record** is a record whose foreign key value references a
> **non-existent** primary key value.

## Using the `orphanRemoval` attribute

We can use the `orphanRemoval` attribute on the `@OneToMany` side as follows:

![lesson 28 02](./screenshots/lesson-28-02.png)

This ensures that when a `Student` is deleted, its associated `Guide` is
deleted, and **any `Student` records associated with that `Guide` are also
deleted**.

Note the deletion order:

1. Student `2` is deleted
2. Student `3` is deleted; would have been orphaned
3. **Then** the `Guide` is deleted
