# Derived Identifiers with `@MapsId`

A **derived identifier** is an ID derived from an **entity**. Derived IDs are
used with single-point associations, i.e. `@OneToMany` or `@ManyToOne`
relationships.

Consider the following diagram:

![lesson 30 01](./screenshots/lesson-30-01.png)

Currently, we are creating a **foreign key** relationship using the
`@JoinColumn` annotation in the `Customer` class. We **could** instead instruct
the `Customer` class to **borrow** the `id` attribute of the `Passport` record
and use it as its **own `id` attribute**:

![lesson 30 02](./screenshots/lesson-30-02.png)

Note the following:

- We have **removed** the `@GeneratedValue` annotation from the `Customer`'s
  `id` attribute

- We have **added** the `@MapsId` annotation to the `passport` attribute

Now, the `Customer` and the `Passport` will share the **same primary key**. This
gives us the ability to **get the `Passport` associated with the `Customer`
using the same `id`**:

```java
Passport passport = (Passport) session.get(Passport.class, 1L);
Customer customerByPassportId = (Customer)session.get(Customer.class, passport.getId());
```

**We can now remove the bidirectional mapping**:

![lesson 30 03](./screenshots/lesson-30-03.png)
