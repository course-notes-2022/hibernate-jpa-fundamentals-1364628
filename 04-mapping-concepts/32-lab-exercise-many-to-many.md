# Lab Exercise: Many to Many Relationship

How can we make the inverse side (i.e. the owned ide, `Actor`), also responsible
for the bi-directional relationship?

![lesson 31 04](./screenshots/lesson-31-04.png)

We saw that updating the Actor to add a Movie did **not** result in a database
update, as the `Actor` is **not the owner** of the relationship.

We can allow the actor to update the relationship by simply adding a new method,
`addMovie`, that **synchronizes the relationship**:

```java
@Entity
public class Actor {
    //...
    @ManyToMany(mappedBy="actors")
    private Set<Movie> movies = new HashSet<Movie>();

    public Set<Movie> getMovies() {return movies;}

    // Add `addMovie`
    public void addMovie(Movie movie) {
        this.movies.add(movie);
        movie.getActors().add(this); // update the relationship
    }
}
```

Now if we try to update the relationship **from the `Actor` side**, the
**updates will work**:

![lesson 32 01](./screenshots/lesson-32-01.png)

We should also create a `removeMovie` method, for when we need to **remove** a
movie from an `Actor` (and an `Actor` from a `Movie`):

```java
@Entity
public class Actor {
    //...
    @ManyToMany(mappedBy="actors")
    private Set<Movie> movies = new HashSet<Movie>();

    public Set<Movie> getMovies() {return movies;}

    public void addMovie(Movie movie) {
        this.movies.add(movie);
        movie.getActors().add(this);
    }

    // Add `removeMovie`
    public void removeMovie(Movie movie) {
        this.movies.remove(movie)
        movie.getActors().remove(this);
    }
}
```

**We should always have these methods in our entities, particularly when
updating a bidirectional relationship**.
