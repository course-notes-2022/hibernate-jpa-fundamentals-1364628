# Aggregation and Composition

Aggregation indicates a relationship between a whole and its parts, for example,
a music band is made up of artists. If the band breaks up, the musicians still
exist and can join other bands.

In a composition relationship, if the **whole** is destroyed, the **parts** are
destroyed along with it. For example, a house has rooms as its parts. When the
house is destroyed, the rooms are destroyed as well.
