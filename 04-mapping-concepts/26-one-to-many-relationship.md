# One-To-Many Relationship

In the previous lessons, we created a **many-to-one** relationship between a
`Student` and a `Guide`. A student could navigate to his guide using the
`getGuide()` getter. But there's currently no way for a `Guide` to navigate to
its associated `Student`: `Student` has a **uni-directional** `@ManyToOne`
relationship with `Guide`.

For a `Guide` to navigate to its `Student`s, we need to have a
**bi-directional** relationship. The `Guide` should maintain a collection of
`students`, and methods to get/set:

![one-to-many: mappedBy](./screenshots/one-to-many-mapped-by.png)

## `mappedBy`

The `mappedBy` attribute tells Hibernate that the bidirectional relationship
between guide and student is mapped by the `guide` argument, and the name of the
attribute on the OTHER (`Student`) side is named `guide`. The `guide` argument
is the **foreign key on the `Student` side**.

`mappedBy` is **required** in a bi-directional relationship.

## Ownership of a Bi-Directional Relationship

In a bi-directional association, **one and only one of the sides** must be the
**owner** of the relationship. The owner is responsible for the association
column(s) update. The **many side** in a One-to-Many bi-directional relationship
is almost always the **owner** side.

## Example

Consider the following example:

![example](./screenshots/example.png)

When we persist a `Guide` entity, we want to persist the associated `Students`
as well.

We could to do the following:

![persist from inverse](./screenshots/persist-from-inverse.png)

where we add the `Student`s to the `Guide`'s `student collection and then
persist the guide object. The result would be:

```
| id  | enrollment_id | name       | guide_id |
| --- | ------------- | ---------- | -------- |
| 1   | 2014AL50456   | Amy Gill   | 1        |
| 2   | 2014JT50123   | John Smith | 1        |

| id  | name        | salary | staff_id    |
| --- | ----------- | ------ | ----------- |
| 1   | Mike Lawson | 1000   | 2000MO10789 |
| 2   | Ian Lamb    | 2000   | 2000IM10901 |
```

Note that this is updating the `Guide` associations from the **`Student`** side,
i.e. the `@ManyToOne` side. **This works**, because the `Student` is the "many"
side.

What about if we try to update from the **inverse end**?

![persist from inverse 2](./screenshots/persist-from-inverse-2.png)

We could _try_ to add the student to the guide's collection of students, but the
**record would not be persisted to the database**:

```
| id  | enrollment_id | name       | guide_id |
| --- | ------------- | ---------- | -------- |
| 1   | 2014AL50456   | Amy Gill   | 1        |
| 2   | 2014JT50123   | John Smith | **1**    |

| id  | name        | salary | staff_id    |
| --- | ----------- | ------ | ----------- |
| 1   | Mike Lawson | 1000   | 2000MO10789 |
| 2   | Ian Lamb    | 2000   | 2000IM10901 |
```

The `Guide` is **not the owner of the relationship**. It only cares about its
own state, and will not update anything on the other side of the relationship.

How can we make a `Guide` responsible about its relationship to a `Student`? By
**adding a methond on the `Guide` that invokes the `setGuide` method of its
associated student**:

![set guide](./screenshots/set-guide.png)

Note that the `addStudent` method accepts a `Student` object as an argument,
and:

1. Adds the `student` to the `students` collection of the `Guide` object

2. Calls the **`Student`'s** `setGuide` method, passing in the `Guide` as an
   argument

## Determining Who is the Owner

If you ever get confused about which entity is the **owner** of the
relationship, remember:

> The **owner** is the entity that is persisted to the table that has the
> **foreign key column**.

In our case, the **student** has the foreign key column:

![owner](./screenshots/owner)
