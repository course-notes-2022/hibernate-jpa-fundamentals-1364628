# Composite Keys

A **composite primary key** is a combination of more than 1 table column that
identifies the uniqueness of a record/database table row.

**Note**: _No matter how good or natural a composite primary key is, it is **not
recommended** for uniquely identifying a record_. Even so, you may still
encounter them in the wild. So let's learn how to create one.

## Creating a Composite Primary Key

To create a composity primary key, you must **create a separate class for the
key** that implements the `Serializable` interface. The class must implement the
`equals` and `hashCode` methods. You must also annotate it with the
`@Embeddable` interface.

You can then create an attribute on the **parent** class (i.e. the class that
will **use** the primary key) with the `@EmbeddedId` annotation, as follows:

![lesson 37 01](./screenshots/lesson-37-01.png)

### Persisting Data

![lesson 37 02](./screenshots/lesson-37-02.png)

## Business Keys are also Not Good Primary Keys

Not only composite keys, but business keys (such as ISBN, SSN, etc.) are **not
recommended for uniquely identifying a record**, because they have a specific
business meaning attached to them.

Prefer **synthetic identifiers**, i.e. IDs with **no business meaning**, when
designing your entities and schemas.

## Composite Foreign Keys

Imagine we have a parent to child, one to many relationship:

![lesson 37 03](./screenshots/lesson-37-01.png)

In this relationship, the **parent** has a composite **primary key**. Therefore,
**each child will have a composite foreign key**.

Composite keys are defined on the association with the `@JoinColumns`
annotation:

![lesson 37 04](./screenshots/lesson-37-04.png)

|                  |               |
| ---------------- | ------------- |
| single column    | `@JoinColumn` |
| multiple columns | `@JoinColumn` |

### Persisting Data

![lesson 37 05](./screenshots/lesson-37-05.png)
