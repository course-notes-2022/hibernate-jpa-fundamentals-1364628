# Component Mapping

Let's map **components**. A **component** is a part of a whole, such that if the
whole is destroyed, so is the part. Each part may belong to only **one** whole.
A component is a contained object persisted as a **value type**.

Observe the following diagram. In this, we are mapping an address component to a
person. The address is the value type, and does _not_ have its own table in the
database. Thus, we have more **classes** than **tables**:

![2 classes 1 table](./screenshots/2-classes-1-table.png)

## Mapping a Component

Observe the following diagram. We create our `Person` class, and mark it as a
persistence entity with the `@Entity` annotation.

We want to create the `address` property as a **component** of the `Person`. We
mark the `Address` class as a **component** with the `@Embeddable` annotation:

![mapping address component](./screenshots/map-address-component.png)

Notice that the component has no identifier attribute. We then add the
`@Embedded` annotation to the `address` property.

Running the code above results in the following record **persisted to the
database**:

```
SELECT * FROM person;

| id  | city    | street        | zipcode | name |
| --- | ------- | ------------- | ------- | ---- |
| 1   | Seattle | 200 E Main St | 85123   | Ruby |
```

## X

Where did the `person` table name come from? We didn't explicitly specify any
schema for it. We didn't specify the table name or column name for any of the
data attributes. How does Hibernate know to create the table and **map the
attributes to the appropriate columns**?

**Hibernate uses reasonable default values not only for `xml-based metadata`,
but for annotation-based mapping metadata**. When it finds mapping information
missing, it uses sensible defaults. By default, it uses the **table name** to
map an entity to a db table, and the **attribute name** to map an attribute to a
**column** in the db.

The second part of the answer is the `hbm2ddl.auto` ("Hibernate Mapping to Data
Definition Language") property:

![hbm2ddl-auto](./screenshots/hbm2ddl-auto.png)

Setting the value to `update` uses the mapping metadata of the `Person` entity
to create and execute the following schema against the `hello-world` database:

```sql
CREATE TABLE person (
    id BIGINT(20) NOT NULL AUTO_INCREMENT,
    city VARCHAR(255) NULL DEFAULT NULL,
    street VARCHAR(255) NULL DEFAULT NULL,
    zipcode VARCHAR(255) NULL DEFAULT NULL,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

```

We can customize the column names by using the `@Column` annotation:

```java
@Embeddable
public class Address {
    @Column(name="address_zipcode") // Maps "zipCode" to column "address_zipcode"; useful if column and attribute
    // have different names
    private String zipCode;
}
```
