# Lab Exercise: Component Mapping

1. Q: If it mattered for your application to uniquely identify an Address
   object, by a database identity for example, would you create it as an
   `Entity` or a `Value Type`? A: **Entity** type

2. Q: Which objects are `Value Types` in the figure below? A: The two `User`
   objects and the `Order` object

![figure 1](./screenshots/component-mapping-ex-q-2.png)

3. Q: Which of the relationships in figure 2 below indicates the Aggregation
   relationship? A: Option **A**

![figure 2](./screenshots/component-mapping-ex-q-3.png)

## When to Use `create` and `create-drop` as the hibernate `hbm2ddl.auto` Property

With `create`, Hibernate will first issue a `DROP` statement to drop the table
if it exists, and then a `CREATE` statement to create it.

`create-drop` works like `create`, Hibernate will **drop the table** once the
`sessionFactory.close()` method is called.
