# Entities and Value Types

Do all persistent classes have their own database identity (primary key value)?

Imagine we have a `users` database table. Each user has a home address and a
billing address. We _could_ store each address as a series of fields on the
user, but we'd _rather_ store each address in an `Address` object _on the
`User`_ as a property:

![user-address](./screenshots/user-address.png)

This is a better design from a code-reuse perspective.

Why are we not adding a unique ID to the address? We would, if the application
cares about identifying an address. Objects that have their OWN db identity are
called **Entity** types. Objects that have no identity are called **Value**
types.

Objects of Value type are dependent on their Entities. We must decide for our
specific applications whether an object should be an entity or value type:

> "Does the database identity of an object matter?"
