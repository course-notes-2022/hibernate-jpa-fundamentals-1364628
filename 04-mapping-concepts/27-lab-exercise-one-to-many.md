# Lab Exercise: One-To-Many Relationship

What if we wanted to map a **uni-directional** one to many relationship? In this
exercise, we'll look at how to do so, and **why we want to _avoid_ doing so**,
as it could have **performance costs**.

Recall from the previous lessons on **bi-directional** relationships (see
below):

for a **bi-directional** one-to-many relationship:

- The **inverse-end** of the relationship is annotated with `@OneToMany`. The
  `mappedBy` attribute is **required**.

- `mappedBy` maps to the **attribute name** of the attribute in the `@ManyToOne`
  class, i.e. the **owner** of the relationship. The **attribute name** maps to
  the **foreign key** column name of the **owner**'s database table.

- The relationship is **bi-directional** because **each side in the relationship
  can navigate to the other side**.

## Making it a Uni-directional Relationship

To make it a uni-directional relationship, we must:

- Remove the `mappedBy` attribute in the `@OneToMany` class

- Remove the attribute in the `@ManyToOne` class _referenced by_ the `mappedBy`
  attribute, **along with all the annotations on the attribute**:

![lab exercise 27 02](./screenshots/lab-exercise-27-02.png)

That's all you have to do. Hibernate will generate a `JOIN` table with the
foreign keys of the `guide` and `student`:

![lab exercise 27 03](./screenshots/lab-exercise-27-03.png)

Note that the column is named `students_id`, _not_ `student_id`. This is
Hibernate inferring the name from the `students` attribute in `Guide`. Hibernate
enforces a `UNIQUE` constraint on `students_id` to avoid duplicate records.

## Prefer Bi-Directional Over Uni-Directional

Why? Because uni-directional relationships create a **join table**, which adds
significant **performance costs** due to the **additional `INSERT` and `JOIN`
statements required**.

Observe the following diagram, which illustrates the **tables created** by a
**bidirectional one to many** and a **unidirectional one to many**:

![lab exercise 27 04](./screenshots/lab-exercise-27-04.png)

Note that the bidirectional **does not** create a join table, but establishes
the relationship betwee and student and guide with a **foreign key in the
`student` table**.

### Persisting Some Data

Let's see this in action. Consider the `Guide` and `Student` entities below,
implementing a **unidirectional** one to many:

![lab ex 27 05](./screenshots/lab-ex-27-05.png)

We can persist a guide and two students as follows:

![lab ex 27 06](./screenshots/lab-ex-27-06.png)

The SQL generated at runtime is shown at the right. Note that we have a **`JOIN`
table created**, and **two `INSERT` statements** for creating the relationships
between the `Guide` and its two `Students`. We **would not need these two
additional statements with a BIDIRECTIONAL relationship** which is not good for
performance!

Let's look at an example of **updating** data:

![lab ex 27 07](./screenshots/lab-ex-27-07.png)

Here, we create a new `Student` and add the student to the existing `Guide`,
resulting in the four statements being created as indicated.

Had we implemented a **bidirectional** relationship, the SQL would have been
much cleaner:

![lab ex 27 08](./screenshots/lab-ex-27-08.png)

## Creating a Separate Database

We can create a new database for this lab.

**IMPORTANT NOTE**: _YOU DO NOT HAVE TO CREATE THE TABLE SCHEMAS MANUALLY.
Hibernate will take care of the table creation. This is the `hbm2ddl.auto`
property in action. In this lesson, it is set to `update`._
