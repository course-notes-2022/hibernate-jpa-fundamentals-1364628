# One To One Relationship

Let's look at a **one to one** relationship. A `Customer` can have a `Passport`,
and a `Passport` can belong to _one_ `Customer`:

![lesson 29 01](./screenshots/lesson-29-01.png)

Recall that in a **bi-directional relationship**, **one entity must be the
"owner"** of the relationship.

The **owner** of the relationship is responsible for the association (i.e. the
**foreign key**) column(s) update.

To declare a side as **not** responsible for the relationship, we use the
`mappedBy` attribute.

**To make the relationship one to one**, in addition to the `@OneToOne`
annotation, we myst use the `unique=true` attribute on the `@JoinColumn`
annotation in the **owner**.

We also add the `cascade=CascadeType.PERSIST` attribute to the **owner**, to
tell Hibernate to persist the **associated objects** along with the `Customer`:

![lesson 29 02](./screenshots/lesson-29-02.png)

`hbm2ddl.auto` to `update` tells Hibernate to create the `Customer` and
`Passport` tables for us.

We persist the `Customer` and `Passport` as follows:

![lesson 29 03](./screenshots/lesson-29-03.png)
