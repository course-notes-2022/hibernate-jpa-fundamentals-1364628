# Cascades

Currently, we're calling two separate methods to save a `Student` and its
associated `Guide`. This is not optimal. We'd prefer to be able to save the
`Student`, and its associated `Guide` _in one method call_. **Cascading** allows
us to accomplish this.

**Cascading** refers to the ability to **persist the entire object graph** in a
single call. JPA enables us to accomplish this using the `cascade` attribute:

![cascade](./screenshots/cascade.png)

We add the `CascadeType.PERSIST` value to the `@ManyToOne` annotation.

We can also cascade the `REMOVE` operation to **delete** the entire object graph
of the `Student` when the student record is deleted. We can also pass **multiple
values** to `cascade`:

![multiple cascade values](./screenshots/multiple-cascade-values.png)

We can delete a student and a guide as follows:

![cascade with client](./screenshots/cascade-with-client.png)
