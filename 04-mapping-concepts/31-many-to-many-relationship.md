# Many to Many Relationship

In this section, we'll discuss **many-to-many** relationships. Consider the
entities `Movie` and `Actor`. A `Movie` can have many `Actors` in it, and an
`Actor` can appear in many `Movie`s.

The `Actor` class can navigate to all the `Movies`, and the `Movie` class can
navigate to all its `Actors`, making the relationship **bidirectional**.

Recall:

- Every bidirectional relationship must have an **owner**.

- The **owner** is responsible for **updating the relationship** in the database

- To declare a side as **not** responsible for the relationship (i.e. **not**
  the owner), we use the `mappedBy` attribute.

- The **value** of the `mappedBy` attribute refers to, and must equal, the value
  of the **association attribute** on the **owner side**

The mapping of a many-to-many relationship is done using a **join table**. We
map the joining in the **owner** entity using the `@JoinTable` annotation:

![lesson 31 01](./screenshots/lesson-31-01.png)

- the `name` attr defines the **name of the table**
- the `joinColumns` attr defines the name of the foreign key for the **owner**
  in the join table
- the `inverseJoinColumns` attr defines the name of the foreign key for the
  **inverse side** in the join table

## Persisting the Data

We define our entities and configuration as follows:

![lesson 31 02](./screenshots/lesson-31-02.png)

and persist the data as follows:

![lesson 31 03](./screenshots/lesson-31-03.png)
