# What is Hibernate?

**Hibernate** is an **Object-Relational Mapping framework** that allows us to
**map Java objects to a relational database**. It also allows us to specify the
configuration we use to connect to the db, and to specify the way Java objects
should be mapped to the tables in the database. Hibernate does the work of
creating the SQL to save/manipulate our DB entities by calling simple Java
methods.
