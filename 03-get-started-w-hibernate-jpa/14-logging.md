# Logging

If we want to see Hibernate's internal workings, we can enable Hibernate's
**logging**. This can help us troubleshoot complex issues with debugging, as
well as output our logs to **log files**.

We'll use the `Log4J` package. Add the `log4j` JAR file to the classpath of your
application.
