# Lab Exercise: `hello world` with Hibernate and JPA Annotations

In this lab exercise, we'll answer the following question:

> "What are the different **states** that the following `Message` object is
> going through during the transaction?"

```java
public class HelloWorldClient {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Message message = new Message("hello");

        session.save(message);

        session.getTransaction().commit();

    }
}
```

By the time line 12 is executed:

```java
session.beginTransaction();
```

we have a `Session` object created, the db connection is established, and we
**begin** the transaction.

By the time line 14 is executed:

```java
Message message = new Message("hello");
```

A `Message` object is created in a **transient state**. This means that its
state will be **garbage collected** as soon as its no longer referenced by any
object. The `Message` object is not yet associated with a DB row.

By line 16:

```java
session.save(message);
```

The message has become a **persistent** object, and is no longer transient. It
is now associated with a DB row. It has a primary key as a unique ID.

By line 18:

```java
session.getTransaction().commit();
```

The transaction is committed and the DB connection is closed. The `Session`
object is **still** managing the `Message` object.

By line 19:

```java
 session.close();
```

The `Message` state becomes **detached**, and is no longer managed by the
`Session`.
