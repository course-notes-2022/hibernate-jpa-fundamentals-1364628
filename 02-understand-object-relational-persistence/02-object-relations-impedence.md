# Object/Relational Impedence Mismatch

In this lecture, we'll discuss **object/relational impedence mismatch**.

Object-oriented languages such as Java represent data as **interconnected graphs
of objects**, whereas RDMS reperesent data in a **table-like format**. Because
both models represent data **differently** when we load or store objects in a
table, we encounter 5 mismatch problems. This is called **Object Relational
Impedence Mismatch**.

## Granularity Mismatch

**Granularity** is the extent to which a system could be broken down into small
parts.

For example, a `Person` is a course-grained object: it contains a lot of details
about the Person: address, name, orders placed, etc. `Address` is a fine-grained
object, because it contains things specific only to an **address**. A
course-grained object consists of multiple **fine-grained object**.

In a Relational model, only 2 levels of granularity exists: **tables** and
**columns**.

![granularity](./screenshots/granularity.png)

## Subtypes Mismatch

We have the concept of subtypes or inheritence in the Object model, but no such
concept in the Relational model:

![subtypes](./screenshots/subtypes.png)

## Sameness Mismatch

In a Relational model, the sameness of two entities is defined **only by their
primary key**. Java defines object **identity** as whether or not two references
point to the **same location in memory**, and **equality** as how the `equals()`
method implements equality:

![sameness](./screenshots/sameness.png)

## Associations Mismatch

Association in the Object model is achieved by **object references** and by
**foreign keys** in the relational model. Object model associations are
**bi-directional**, and **non-directional** in relational associations:

![associations](./screenshots/associations.png)

## Data Navigation

In Java, the way we access data is fundamentally different than what we do in a
database. To find a particular item we navigate from one object to another until
we reach the item.

For example, if we want to get the value of `z` starting from `foo`, we have to
do:

> `foo.getBar().getY().getZoo().getZ()`

In a relational database, we minimize the number of queries using **join
queries** when we want to get data from more than one table:

![data navigation](./screenshots/data-navigation.png)
