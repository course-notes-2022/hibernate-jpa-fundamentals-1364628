# Install MySQL

Let's install MySQL for our RDBMS that we'll be using in this course. If you
already have a favorite RDBMS, you can continue to use that one.

**NOTE**: _The lecture installs MySQL on a Windows machine.
[See here for an article describing how to install on Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-20-04)_.

## Create and Use a New Database

Use the following SQL statements to create the `bookstore` database and
associated tables:

```sql
CREATE DATABASE IF NOT EXISTS bookstore;

USE bookstore;

CREATE TABLE publisher(
    code VARCHAR(4) NOT NULL PRIMARY KEY,
    publisher_name VARCHAR(100) NOT NULL
);

CREATE TABLE book (
    isbn VARCHAR(50) NOT NULL PRIMARY KEY,
    book_name VARCHAR(100) NOT NULL,
    publisher_code VARCHAR(4),
    FOREIGN KEY (publisher_code) REFERENCES publisher(code)
);

CREATE TABLE chapter (
    book_isbn VARCHAR(50) NOT NULL,
    chapter_num INT NOT NULL,
    title VARCHAR(100) NOT NULL,
    PRIMARY KEY (book_isbn, chapter_num),
    FOREIGN KEY (book_isbn) REFERENCES book (isbn)
);
```

## Install HeidiSQL (Optional)

HeidiSQL is a GUI tool that allows us to connect to our MySQL server.

## Download the MySQL JDBC Driver

[Downlod the MySQL JDBC Driver for Ubuntu Linux here](https://dev.mysql.com/downloads/connector/j/).
