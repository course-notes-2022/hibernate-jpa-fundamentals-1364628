# Lab Exercise: Object Relational Mapping

For this exercise, the only task we'll do is:

- Task 0: Download the source code, run it, and make yourself comfortable with
  the concepts covered in it

- Task 1: What will happen if you execute the code in the `BookStoreClient`
  class **twice**?

- Task 2: Can you figure out the type of **object-relational impedence
  mismatch** depicted in the following diagram?

![task 2](./screenshots/task-2.png)
