# Lab: Object Relational Mapping

Let's begin developing the bookstore application. Ensure that MySQL is running
on your system:

> `sudo systemctl start mysql.service`

You can enter the MySQL shell by running:

> `mysql -u root -p`

NOTE: It's bad practice to continue logging in as the `root` user. Consider
creating a **new user** with the following command:

> `CREATE USER 'sammy'@'localhost' IDENTIFIED BY 'password';`

Create the `bookstore` database and tables if you haven't already done so using
the script in the previous lesson.

Verify that the tables have been created with the `SHOW TABLES` command:

```
mysql> SHOW TABLES;
+---------------------+
| Tables_in_bookstore |
+---------------------+
| book                |
| chapter             |
| publisher           |
+---------------------+
3 rows in set (0.00 sec)

```

You can view the table schema with the `DESCRIBE <TABLENAME>` command:

```
mysql> DESCRIBE book;
+----------------+--------------+------+-----+---------+-------+
| Field          | Type         | Null | Key | Default | Extra |
+----------------+--------------+------+-----+---------+-------+
| isbn           | varchar(50)  | NO   | PRI | NULL    |       |
| book_name      | varchar(100) | NO   |     | NULL    |       |
| publisher_code | varchar(4)   | YES  | MUL | NULL    |       |
+----------------+--------------+------+-----+---------+-------+
3 rows in set (0.00 sec)

```

Let's now create the classes in our project. Create a new Java project,
`bookstore` in your IDE of choice, and copy the files attached to the lesson to
your new project. Once copied successfully, run the `BookStoreClient.java` file
to **populate your database with test data**. Verify that the data has been
saved to the database correctly:

```
mysql> SELECT * FROM book;
+---------------+-------------------------------------------------+----------------+
| isbn          | book_name                                       | publisher_code |
+---------------+-------------------------------------------------+----------------+
| 9781617290459 | Java Persistence with Hibernate, Second Edition | MANN           |
+---------------+-------------------------------------------------+----------------+
1 row in set (0.00 sec)

mysql> SELECT * FROM chapter;
+---------------+-------------+-------------------------------+
| book_isbn     | chapter_num | title                         |
+---------------+-------------+-------------------------------+
| 9781617290459 |           1 | Introducing JPA and Hibernate |
| 9781617290459 |           2 | Domain Models and Metadata    |
+---------------+-------------+-------------------------------+
2 rows in set (0.00 sec)


mysql> SELECT * FROM publisher;
+------+--------------------------+
| code | publisher_name           |
+------+--------------------------+
| MANN | Manning Publications Co. |
+------+--------------------------+
1 row in set (0.00 sec)


```

## Problems with Using JDBC Directly

The problems we face when using JDBC **directly** to persist/retrieve our data
is due to the **mismatch** of the object and relational database models. For
example, we are using a **foreign key relationship** to reference a **book
record** from a **chapter record**:

![book to chapter](./screenshots/book-to-chapter.png)

But there is nothing references the `Book` class from the `Chapter` class.
Instead, we have a `Book` that has many `Chapters`:

![book has many chapters](./screenshots/book-has-many-chapters.png)

**This is an example of object relational mismatch**. To handle the
incompatibility, we have to do some conversion, which is object relational
mapping. With JDBC, it's a **lot of work**. Hibernate provides a better way!
