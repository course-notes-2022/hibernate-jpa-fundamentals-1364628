# Object-Relational Mapping

Let's learn about **object-relational mapping** with an example of a book store:

![book store app diagram](./screenshots/bookstore-app-diagram.png)

We'll have 3 classes in our application:

- `Book`
- `Publisher`
- `Chapter`

and 3 tables in our database:

- `PUBLISHER`
- `BOOK`
- `CHAPTER`

![bookstore classes and tables](./screenshots/bookstore-classes-tables.png)

Our classes are simple Java POJOs. All classes define attributes, getters and
setters, and a `toString()` method.

Our bookstore database model is as follows. Note that we're using a **composite
key** of the ISBN and chapter number to uniquely identify a `Chapter`:

![bookstore db model](./screenshots/bookstore-db-model.png)

## Saving a Chapter to the Database: The HARD Way

Imagine we have a `BookStoreService` class that defines two methods for
pesisting and retrieving a Book. Our service is leveraging the `JDBC` driver for
MySQL to allow java to connect to MySQL. The
`mysql-connector-java-x.y.z-bin.jar` needs to be on our `CLASSPATH`.

### Using `BookstoreService` to Cteate a `Book` Record

We first create an new `Publisher`, then a `Book`, a list of `Chapters`, then
set the `chapters` of the `Book` to the value of the `Chapters` list, then
persist the `Book`:

[bookstore client](./screenshots/bookstore-client.png)

To achieve all the saves, we'd need to write code like the following:

![insert with jdbc](./screenshots/insert-with-jdbc.png)

There are several problems with this approach:

1. We are writing the SQL statements **manually**, and **many** statements are
   required for just a **simple insert**

2. There are too many code duplications

3. The SQL code we have written is **MySQL database specific**; what if we want
   to switch to a **new database later on**?

## Solution: Object Relation Mapping (ORM)

**Object Relational Mapping** refers to the technique of mapping the
represenation of data from **Java objects** to a **relational database** and
vice-versa. ORM allows you to use **Java objects** as represenations of a
relational database. It maps between the POJOs and the relational database. This
mapping is done either by `xml` or **annotations**, hiding the complexity of
`SQL` and `JDBC`:

![orm in action](./screenshots/orm-in-action.png)
